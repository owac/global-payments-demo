# SOV DEMO App.

## Links

* [Test](https://shinydevint.myowg.com:9011//shiny/actuarial/shipyard/sov-demo/)
* [Prod](https://oliverwymanapps.mmc.com/actuarial/shipyard/sov-demo/)
* [Data](https://mmcglobal-my.sharepoint.com/:f:/r/personal/bryce_chamberlain_oliverwyman_com/Documents/Projects/global-payments-sov?csf=1&web=1&e=Ucewcx)

## Prerequisites

Complete the following before using these files, if you haven't already:

* Install R [https://www.r-project.org/]
* Install RStudio [https://www.rstudio.com/products/rstudio/download/] (Desktop, Open Source version, 64-bit is better).
* Install Git. There are two options for this. The first is recommended.
    * 1: OWAC pre-specified install - in Windows search, find "Software Center". Under Available Software find the one with Python, Pycharm, and Git in the name. Check it and INSTALL SELECTED.
        * If it is listed under Installed Software, you already have it installed.
    * 2: From Git [https://git-scm.com/book/en/v2/Getting-Started-Installing-Git]. Use defaults, except select "Use Git from Git Bash Only".
    * Optional: Install SourceTree [https://www.sourcetreeapp.com/]. SourceTree is a more user-friendly Git user interface but is slower and sometimes can't be installed on OW machines.
* Install Visual Studio code 64-bit https://code.visualstudio.com/download. Make sure to choose **Add Open This Folder to Context Menu** or similar option - this is not the default.

## Important Notes About the Code

* The app as-is has a charts working off base R datasets, in order to provide a working generic model to start from.
* I use loops to automatically include files. You'll notice files starting with "s-", this is how [server files](https://shiny.rstudio.com/articles/two-file.html) are identified. [UI files](https://shiny.rstudio.com/articles/two-file.html) are identified with "u-". This convention makes sure the right files are run at the right time so that you don't have to add new code every time you add a file.
* You'll notice files starting with just "-" or not ending in ".R", these are unfinished files that the app ignores. I've copied these from other apps but haven't yet generalized and implemented them. You may find the code useful if you want to build a specific type of chart.
* Any javascript in js/ or css in css/ will be picked up automatically by the app (via a loop in u-head.R).
* I like to set up a folder for each tab and put both ui and server files in that folder so all the files for the tab are in one place.
* The app is modified to use [Font Awesome](https://fontawesome.com/icons) version 5 as opposed to default shiny which uses 3. 5 is more modern and aligns better with the Font Awesome website for searching icons.

## Features of the Code

* There are javascript files that manage resizing the view based on screen size, converting things to images, formatting chart labels, etc. If you don't want the app resized to fit each new screen, delete www/js/autozoom-tooltip.js.
* Custom selectize inputs with include/exclude button are built automatically from the filters object defined in data prep. See data-prep/scripts/#-filters.R.
* Functions proginit, proginc, and progclose manage progress bars. See 0-general/s-progress.R.
* l() function prints to console locally only, which keeps server logs clean and easier to pull errors from.

## Dev Notes

Code Features Unique to this App:

* If you get an error about recursive/stack issues with no information, comment out logging in the exec() and query() functions.

*filter strings*

* Function `viz()` `app\0-general\s-viz.R` sets up div with class filter-note and runs javascript to update the filter string to the default value from `app\www\js\filterstring.js `.
* Function `filter_setnote()` `app\0-general\s-filterinfo.R #127` runs to update this string when the function `filters_calcstring()` is run.
* A good place to run filters_calcstring is at the base reactivies in `app\0-general\s-data.R` which run any time filters change.
* Change filter note formatting via CSS class `.filter-note`

## Functionality Test

Statement of Values:

* Filter by Address
* Click Clear Filters.
* Filter by Alarm System = Both.
* Click Clear Filters.
* Hide a column using "Click to toggle columns"
* Change currency to CAD
* Download data. Make sure the currency is in the headers.
* If on dev:
    - Create a new record (only need to set Base Currency = USD).
    - Edit an existing record.
    - Note numbers, change View Currency, ensure numbers change and value headers show new currency. 

Change History:

* Apply filter on Record ID and Modified from/through.
* Look at both views: All Fields, Change in Values
* Download data.
