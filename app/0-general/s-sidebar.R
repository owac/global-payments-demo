# This code manages the sidebar as things change.


# Show/hide sidebar filters by the tabset.

  observeEvent( input$tabset, {
    
      for( i in filters ){
        
          idivid = paste0( i$col, '_div' )
          
          if(filterApplies(i$col)){  shinyjs::show(idivid) } else { shinyjs::hide(idivid) }
          
  }})

# Clear button click.

  observeEvent( input$SB_clearButton, {
  
      lo( 'observeEvent( input$SB_clearButton' )
  
      # Clear any selected filters.
      for( i in filters ) updateSelectizeInput( session, i$col, selected = 'clear' )
      updateTextInput(session = session, inputId = 'address', value = '')
    
      # www/js/include-exclude.js handles restting the include/exclude buttons.
      
  })