# set up userfolder and dbfile in global.R.
if(!dir.exists(userfolder)) dir.create(userfolder)

# restrict characters and value check.
sqlvalcheck = function(x){
    if(grepl(' |where|delete|select|alter|create|drop', tolower(x))) stop('
        SQL value check failed, you may need to modify a name or id. Error E1212.
    ')
    return(x)
}
sqlrestrict = function(x){
  
  x = gsub('[^][A-Za-z0-9 ?(),_=-]', '', x)
  x = gsub(' +', ' ', x)

  # items that allow injections. see https://github.com/unicornsasfuel/sqlite_sqli_cheat_sheet.
  x = gsub('randomblob|sleep|load_extension|true|false|attach|pragma|substr|cast', '', x)

  return(x)

}

doconnect = function(read_write_create) dbConnect(
    RSQLite::SQLite(), 
    dbfile,
    loadable.extensions = FALSE,
    flags = c(
        'read' = RSQLite::SQLITE_RO,
        'write' = RSQLite::SQLITE_RW,
        'create' = RSQLite::SQLITE_RWC
     )[read_write_create],
     default.extensions = FALSE
)

dbinfo = function(){
  
  if(!file.exists(dbfile)) stop(glue('sqlite file not found: [{dbfile}].'))
  conn = dbConnect(RSQLite::SQLite(), dbfile)
  
  idt = tryCatch({
     tables = dbListTables(conn)
     tables_info = lapply(tables, function(x){
       icolumns = dbListFields(conn, x)
       list(
         columns = icolumns,
         ncols = length(icolumns),
         nrows = query(cc('select count(*) as nrows from ', x))$nrows
       )
     })
     names(tables_info) = tables
     list(
       tables = tables_info
     )
  }, error = function(e) warning(as.character(e)))
  
  dbDisconnect(conn)
  
  return(idt)
  
}

query = function(command, params = NULL){

    if(!file.exists(dbfile)) stop(glue('sqlite file not found: [{dbfile}].'))

    conn = doconnect('read')

    idt = tryCatch({
      q = dbSendQuery(conn, command)
      if(!is.null(params)) dbBind(q, params)
      v = suppressWarnings(dbFetch(q)) # warns about data types, but it gets the right types so this can be ignored.
      dbClearResult(q)
      v
    }, error = function(e){
      dbDisconnect(conn)
      warning(as.character(e))
    })

    dbDisconnect(conn)

    return(idt)

}

exec = function(command, params = NULL, read_write_create = 'write', verbose = TRUE) {

    if(!file.exists(dbfile) && !grepl('CREATE', command)) stop(glue('sqlite file not found: [{dbfile}].'))
    conn = doconnect(read_write_create)

    # restrict characters.
    command  = sqlrestrict(command)
    if(verbose) lo(cc('exec: ', command))

    # any date parameters will convert to numbers and cause issues.
    # replace dates with strings.
    for(i in 1:length(params)) if(is.Date(params[[i]])) params[[i]]<- as.character(params[[i]])

    info = tryCatch({
        statement = dbSendStatement(conn, command)
        if(!is.null(params)) dbBind(statement, params)
        info = list(
            Statement = trimws(gsub('\n', '', gsub('  ', ' ', dbGetStatement(statement)), fixed = TRUE)),
            ColumnInfo = dbColumnInfo(statement),
            RowsAffected = dbGetRowsAffected(statement),
            RowCount = dbGetRowCount(statement),
            HasCompleted = dbHasCompleted(statement)
        )
        dbClearResult(statement)
        info
    }, error = function(e){ 
        dbDisconnect(conn)
        stop(e)
    })

    dbDisconnect(conn)

    return(info)

}

insert = function(table, data, verbose = TRUE) {

    # convert to character matrix.
    data = as.data.frame(data) # might come in as a list.
    cols = names(data)
    data %<>% fac2char()
    #types = sapply(data, function(x) class(x)[1])
    data = as.matrix(data, ncol = ncol(data), nrow = nrow(data))

    # add " to strings.
    #for(i in which(types %in% c('character', 'POSIXlt', 'POSIXct', 'Date'))) data[, i] = cc("'", gsub("'", "''", as.character(data[, i])), "'")

    # in order to handle multiple rows, we set up parameters for each row:
    values_onerow = cc(rep('?', ncol(data)), sep = ', ')
    values_allrows = cc('(', cc(rep(values_onerow, nrow(data)), sep = '), ('), ')')
    
    table = sqlvalcheck(table)

    # and then grab paramaters from the data, ensuring they match the order of values_allrows
    params = list()
    for(irow in 1:nrow(data)) for(icol in 1:ncol(data)) params[[length(params) + 1]] <- data[irow, icol]

    command = as.character(glue("
        INSERT INTO {table} ([{cc(cols, sep = '], [')}])
        VALUES {values_allrows};
    "))

    exec(command, params, verbose = verbose)
    
}

update = function(table, where, data, params, verbose = TRUE){
  
    data = as.data.frame(data, make.names = FALSE) # might come in as a list.

    set = cc(sapply(names(data), function(key){
        val = data[[key]]
        glue('[{key}] = ?')
    }), sep = ", ")

    allparams = lapply(data, function(x) x[1])
    names(allparams) <- NULL
    for(i in params) allparams[[length(allparams) + 1]] <- i

    command = as.character(glue('
      update {table} 
      set {set} 
      where {where}'
    ))
    
    exec(command, allparams, verbose = verbose)

}

builddatabase = function() if(!exists('stopbuild')){

     if(!file.exists(dbfile)){

        # create data_objects_log:
        exec('
          CREATE TABLE data_objects_log (
            id INTEGER PRIMARY KEY AUTOINCREMENT, 
            created DATETIME,
            table_name TEXT,
            table_id INTEGER,
            desc TEXT,
            people_id INTEGER
        )', read_write_create = 'create')

        # create other tables:
    
        prototype = data.frame(
            name = character(),
            dates = as.Date(character()),
            active = logical(),
            value = numeric()
        )
        
        typeswap = c(
            'character' = 'TEXT',
            'factor' = 'TEXT',
            'ordered' = 'TEXT',
            'Date' = 'DATETIME',
            'integer' = 'INTEGER',
            'numeric' = 'REAL',
            'logical' = 'INTEGER'
        )
        
        data_objects$apply(function(x){

            # validate parents
            tablesnotfound = setdiff(x$parents, names(data_objects))
            if(length(tablesnotfound) > 0) stop(glue('Tables not found: [{cc(tablesnotfound, sep = ", ")}].'))
            
            fields = cc(names(x$prototype), sapply(x$prototype, function(x) typeswap[[class(x)[1]]]), sep = ' ')
            fields = cc(fields, sep = ', ')
            
            if(length(x$parents) > 0){
              parents = cc(cc(x$parents, '_id INTEGER'), sep = ', ')
              combine = if(fields != ''){ cc(parents, fields, sep = ', ') } else { cc(parents, sep = ', ') }
            } else {
              combine = fields
            }
            
            createsql = glue('CREATE TABLE {x$id} (
                id INTEGER PRIMARY KEY AUTOINCREMENT, 
                created DATETIME, 
                created_by INTEGER,
                modified DATETIME,
                modified_by INTEGER,
                totrash DATE,
                totrash_by INTEGER,
                {combine}
            );')
            
            exec(createsql, read_write_create = 'create')

            # people should be added first. then we need to add the current user. 
            if(x$id == 'people'){           
      
              # we need the current person to be in the database for downstream operations. 
              data_objects$insert(
                'people', 
                data.frame(
                  email = iuser$user
                )
              )
              people <<- query('select * from people where email = ?', list(iuser$user))

            }

            # load starting data if it exists.
            if(!is.null(x$starting_data)){
              proginit('Creating initial database.')
              for(row in x$starting_data %>% split(1:nrow(.))){
                proginc()
                # checkempty = FALSE since incoming data is not required to be complete.
                data_objects$insert(x$id, row, verbose = FALSE, checkempty = FALSE)
              }
              progclose()
            }

        })

    }

    # add the user if they aren't in the database yet.  

      people = query('select * from people where email = ?', list(iuser$user))
      
      # if the person isn't in the database yet, add them.
      if(nrow(people) == 0) data_objects$insert(
        'people', 
        data.frame(
          email = iuser$user
        )
      )
      people = query('select * from people where email = ?', list(iuser$user))

      if(nrow(people) > 1) stop('Found multiple accounts. Error E944-1')
      people <<- people
      
}

people = NULL
builddatabase()