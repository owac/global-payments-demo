var filterinfo = {};

function set_filterinfo(filter_id, text){
    filterinfo[[filter_id]] = text;
}

function update_filter_ui(viz_id, filter_id){
    if(filter_id == 'none') return;
    if(filterinfo[filter_id]){
        $("#" + viz_id + "-container .filter-note").html(
            "<p class='helper_subscript'>" + filterinfo[filter_id] + "</p>"
        ); 
    }
}

function update_filter_ui_all(filter_id){
    if(filter_id == 'none') return;
    $("." + filter_id).html(
        "<p class='helper_subscript'>" + filterinfo[filter_id] + "</p>"
    ); 
}
