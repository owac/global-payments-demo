// addCustomMessageHandler linking to app/0-general/s-utils.R

Shiny.addCustomMessageHandler('alert', function(message){ 
    alert(message);
});

Shiny.addCustomMessageHandler('click', function(message){ 
    document.getElementById(message).click();
});

