$( document ).ready( function() { 

    // Allow reorder of UL elements. Use class sortable if you actually want to implement this, otherwise this does nothing.
    // See https://jqueryui.com/sortable/.

        $(".sortable").sortable();
        $(".sortable").disableSelection();
        $('.sortable').sortable({ cancel: '.selectize-control, .selectize-dropdown-content, .viz-wrapper' });

});