function columnselector_click(col){
    $('#columnselector_' + col).toggleClass('deselected');
    Shiny.onInputChange("sovtable_columnselector_click", col);
}

function toggle_sovtable_columnselector(){
    $('#sovtable_columnselector_selector').toggleClass('hidden');
    $('#toggle_sovtable_columnselector').empty();
    if($('#sovtable_columnselector_selector').hasClass('hidden')){
        $('#toggle_sovtable_columnselector').append('<i class="fas fa-angle-right"></i>');
    } else {
        $('#toggle_sovtable_columnselector').append('<i class="fas fa-angle-down"></i>');
    }
}

