// Code to help manage include/exclude filters.

    // Run this code after all elements are loaded.

        $( document ).ready(function() { 

            // Set toggle selected class on exclude filter buttons.
            $( '.inex button' ).each( function( i, ielem ){
                $(ielem).click( function(){ 
                    $(this).toggleClass('selected'); 
                    $(this).blur(); // remove focus from the button so it doesn't show dark.
                });
            });

            // Set the clear button to also clear/click the exclude filters.
            $('#clearButton').click(function(){
                
                $( '.inex button' ).each( function( i, ielem ){
                    if($(ielem).hasClass('selected')){
                        $(this).click(); // this will trigger the class change and update the button in Shiny.
                        $(this).blur(); // remove focus from the button so it doesn't show dark.
                    }
                });

            });

        });