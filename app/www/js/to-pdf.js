// Follows https://stackoverflow.com/a/19290964.
// Requires files: jspdf.js; from_html.js; split_text_to_size.js; standard_fonts_metrics.js (these should be loaded automatically by ui_head).
// I did have to make some manual changes to from_html.js - replace .getWidth() with .width, etc. Run it 'till error and fix each error 1 at a time (there was 3).
      
/*var elementHandler = {
  '#kpi_table_download': function (element, renderer) { return true; }
};
function toPdf(elementid){
    
    var source = window.document.getElementById(elementid);
    var pdf = new jsPDF();

    margins = { top: 5, bottom: 5, left: 5, width: 1600 };

    pdf.fromHTML(
        source, // HTML string or DOM elem ref.
        margins.left, // x coord
        margins.top, { // y coord
            'width': margins.width, // max width of content on PDF
            'elementHandlers': elementHandler
        },

        function (dispose) {
            // dispose: object with X, Y of the last line add to the PDF 
            //          this allow the insertion of new lines after html
            pdf.save('Test.pdf');
        }, margins
    );
    
}*/

// https://github.com/eKoopmans/html2pdf
/*function toPdf(elementid){
    var element = document.getElementById(elementid);
    html2pdf(element, {
        margin:       0,
        filename:     'myfile.pdf',
        image:        { type: 'jpeg', quality: 0.98 },
        html2canvas:  { dpi: 192, letterRendering: true },
        //jsPDF:        { unit: 'in', format: 'letter', orientation: 'portrait' }
        jsPDF:        { unit: 'in', format: 'letter', orientation: 'landscape' }
    });
}*/

// I gave up and topdf will now just save as image. I'm leaving this code here though in case we want to try again.

    function toPdf(elementid){
        saveImg( elementid,'table', 'png' )
    }