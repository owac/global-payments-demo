function downloadviz(id){ Shiny.onInputChange("trigger_download", id); }


filterinfo = {}
function updatefilter(filterid){
    if(filterid == 'none') return;
    if(filterinfo[filterid]){
        $("." + filterid).each(function(){ 
            $(this).html( "<p class='helper_subscript'>" + filterinfo[filterid] + "</p>"); 
        });
    }
}

function set_filterinfo(id, value){
    filterinfo[id] = value;
}

// add save as image buttons, including handlers. 
$(document).on( 'shiny:visualchange', function(event){ 
        
    $('.active .save-div').each( function(i,elem){

        var sub = $(elem).find('.chart-sub');

        // brian asked to remove these buttons.

        // handle convert-to-image click
        /*sub.find('.icon_save_container').remove();
        sub.append( '<div class="icon_save_container "><i class="fas fa-camera"></i></div>');
        sub.find( '.icon_save_container' ).click( function(){
            Shiny.onInputChange('convert_to_image', elem.outerHTML); 
        });*/
        
        // handle convert-to-pdf click
        /*sub.find('.icon_pdf_container').remove();
        sub.append( '<div class="icon_pdf_container"><i class="fas fa-file-pdf"></i></div>');
        sub.find( '.icon_pdf_container' ).click( function(){
            Shiny.onInputChange('convert_to_pdf', elem.outerHTML); 
        });*/

    });

});
