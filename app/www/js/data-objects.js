Shiny.addCustomMessageHandler('objectmodalerror', function(message){ 
    $("#objectmodal-error").removeClass("hidden").html(message);
});

function clickedit(infoboth){
    let info = infoboth.split('-');
    console.log(info);
    console.log(infoboth);
    Shiny.onInputChange('clickedit', info);
}

function clickdelete(infoboth){
    let info = infoboth.split('-');
    Shiny.onInputChange('clickdelete', info);
}
