Highcharts.setOptions({
    chart: {
        style: {fontFamily: "OpenSans"}, 
        height: 350, 
        animation: false
    },
    title: {text: ""},
    plotOptions: {
        series: {

            animation: false, 
            stickyTracking: false,
            groupPadding: .1,
            pointPadding: 0,
            label: {enabled: false}, // hides labeling on charts, this should happen in titles or the legend.

            dataLabels: {
                style: {
                    fontWeight: "normal", 
                    fontSize: "10pt", 
                    fontFamily: "Rubik", 
                    textOutline: "0px"
            }},

            // part of disabling tooltip. use enable_tooltip to turn it back on.
            enableMouseTracking: false
        }
    },
    legend: {
        enabled: false, verticalAlign: "top", 
        //width: "100%",  // this can break vertical layout legends.
        itemStyle: {fontWeight: "normal", fontSize: "10pt"}
    },
    xAxis: {
        lineWidth: 0, lineColor: "#707c80",
        gridLineWidth: 0,
        tickLength: 0,
        startOnTick: true, endOnTick: true,
        title: {enabled: false, style: {fontFamily: "WorkSans", fontSize: "11pt"}},
        labels: {style: {fontWeight: "normal", fontSize: "10pt"}},
    },
    // y axis is hidden by default since we use datalables to show numbers instead.
    // cancel this by using enabled = TRUE in charts.
    // consider also moving y axis labels into the chart title, that's a better place often.
    yAxis: {
        // min: 0, <- this breaks maps!!
        endOnTick: false, 
        lineWidth: 0, lineColor: "#707c80",
        gridLineWidth: 0,
        title: {style: {fontFamily: "WorkSans", fontSize: "11pt"}, enabled: false},
        labels: {style: {fontWeight: "normal", fontSize: "10pt"}, enabled: false}
    },
    // we turn off tooltip to run faster and because a good tooltip often takes extra care. 
    // use enable_tooltip to turn it back on.
    tooltip: {enabled: false},
    lang: {
        thousandsSep: ",", 
        drillUpText: "<", 
        numericSymbols: ["k", "M", "B", "T", "P", "E"]
    },
    credits: {enabled: false}
});

