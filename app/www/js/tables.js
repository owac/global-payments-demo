function initComplete(){

    // handle total row.
    var lastrow = $(this).find("tr:last-child");
    if(lastrow.find("td:first-child").html() == "Total"){
        lastrow.addClass("totalRow");
    }

    // handle total column.
    if($(this).find("thead > tr:first-child > th:last-child").html() == "Total"){
        $(this).find("tr > td:last-child").addClass("totalCol");
    }

}

// add double scroll.
var running = false;
function fixtable(id){

    // add divs for scrolling.
    const id_bot = '#' + id + '_scrollbot';
    const id_top = '#' + id + '_scrolltop';
    if($(id_bot).length == 0){
        $("#" + id + " table.display.dataTable").wrap('<div id="' + id + '_scrollbot" class="scrollbot"><div class="subscroll"></div></div>');
        $(id_bot).before('<div id="' + id + '_scrolltop" class="scrolltop"><div class="subscroll"></div></div>');
    }

    // set up double-scroll.

        $(id_top).unbind("scroll");
        $(id_top).scroll(function(){
            if(running) {
                running = false;
                return;
            }
            running = true;
            $(id_bot).scrollLeft($(id_top).scrollLeft());
        });

        $(id_bot).unbind("scroll");
        $(id_bot).scroll(function(){
            if(running) {
                running = false;
                return;
            }
            running = true;
            $(id_top).scrollLeft($(id_bot).scrollLeft());
        });

};

function fixwidth(id){
    $('#' + id + '_scrolltop > .subscroll').css('width', $('#' + id + '_scrollbot > .subscroll > .dataTable').outerWidth() + 'px');
}

function fixtable_sovtable(){
    fixtable('sovtable');
    setTimeout(fixwidth('sovtable'), 100); // width isn't correct until just after the table is initialized.
}

function fixtable_sovhist(){   
    fixtable('sov_hist'); 
    setTimeout(fixwidth('sov_hist'), 100);
}
