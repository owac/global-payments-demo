get_current_rates = function(need_symbols = c()){

    # load data (and build data file if there is none yet).
    filepath = glue('{userfolder}/sov-demo-currencyconversions.RDS')
    if(!file.exists(filepath)){
        dt = list()
        saveRDS(dt, filepath, version = 2)
    } else {
        dt = readRDS(filepath)
    }

    # if we already have data, return it.
    need_date = format(Sys.Date() - 1)
    if(need_date %in% names(dt)) return(dt[[need_date]])

    # if not, call the API.
    response = httr::GET(
        url = glue('https://data.fixer.io/api/{need_date}?access_key=ac41de7a97ad9d40c7caa9cae4435aad&symbols={cc(dosymbols, sep = ",")}'), 
        encode = 'json',
        verbose()
    )
    response = jsonlite::fromJSON(content(response, as = 'text'))
    
    if(!response$success) stop(glue('fixer.io error: {response$error$type}. Error E511.'))

    dt[[need_date]] <- response$rates
    saveRDS(dt, filepath, version = 2)

    return(dt[[need_date]])

}

