If you have any raw data, place it in this folder.

You can tell git via .gitignore to ignore these files so your project doesn't share raw data or include large data files, which can be problematic with git.

This app only uses 1 external data file which is necessary for using a map. It comes from http://code.highcharts.com/mapdata/, find the map you want, click javascript.