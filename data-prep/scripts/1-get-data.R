if( ! cache.ok( 1 ) ){

  # As a test, we'll use the state data that R comes with by default. See http://stat.ethz.ch/R-manual/R-devel/library/datasets/html/state.html.
  data = read.any('raw-data/sov_working_2021.xlsx')

  # fix names.
  data %<>% select(
    
    address = `Street Address`, 
    city = City, 
    state = `State/ Province`, 

    domestic_international = `Domestic/ International`, 
    loc_num = `Chubb Location No.`, 
    entity = ENTITY, 
    building = `Building/ Facility Name`, 
    zip = `Postal Code`, 
    county = County, 
    country = Country, 

    value_building = `Building Value (USD) - Replacement Cost`, 
    value_contents = `Contents Value (USD) - Furniture & Inventory - Replacement Cost`, 
    value_software = `Software Value (USD) - Software - Replacement Cost`,
    value_edp_equip = `EDP Equipment Value (USD) - Computer Equipment - Replacement Cost`, 
    #value_total_insurable = `Total Insurable Value (USD)`, 
    
    employees = `Number of Employees`, 
    owned_leased = `Building Owned or Leased?`, 
    sqft = `Square Footage`, 
    num_buildings = `Number of Buildings`, 
    sqft_entire = `Sq. Footage of Entire Building(s)`, 
    sqft_leased = `Sq. Footage of Leased Space`, 
    floor = `Floor # Occupied`, 
    construction_type = `Building Construction Type`, 
    year_built = `Year Built`, 
    year_renovated = `Year Renovated`, 
    stories = `Number of Stories in Building`, 
    sprinklers = `Sprinklers?`, 
    alarm = `Alarm System - Central or Local`, 
    flood = `Flood Zone`, 
    notes = `Notes` 
  )

  # then save the cache.
  # save.cache will take the cache number from cache.ok so it knows where to save.
  save.cache( data )
  
}